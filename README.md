![Project Tour](https://static.nellsavedra.com/img/alea/tour.gif?)
## Alea Frontend Technical Assessment

Knowledge test for the Senior Front-End Engineer position at Alea

### Stack
- Next.js with App Router routing (i have experience with Pages Router but i decided to give it a try)
- Sass for styling (for years my prefered, lightspeed at writing Sass)
- TanStack Query, new to me, already loving it
- Cypress for testing

## Installation

First things first!, clone the repo to your machine

```bash
git clone https://github.com/nellsavedra/alea-frontend.git
```

## Usage
Install dependencies

```python
npm install
```
When it's done we are ready to run our local server
```
npm run dev
```
This should open a browser window at localhost:3000

## Roadmap
- [x] Create Login Page
- [X] Create Logout Action
- [X] Create User Table at path /users
- [x] Allow changing param per_page [10,5,2,1]
- [ ] Unit Testing
- [x] Create a custom 404 Error Page

## Decisions
- **Why Next.js?**  
By far the most used React Framework, as stated before, i have experience with Pages Router, App Router is relatively new but a challenge is a challenge!, i decide to used it and that way learn how it works.
- **Why Sass?**  
Yes, i know, styled-components its a thing right now but for fast development i prefer Sass, with a big design system and granular components, styled-compoments make more sense.
- **Why TanStack Query?**  
In the interview with Edu he told me that the Frontend team uses it, so...

## Draft
![Project Diagram](https://static.nellsavedra.com/img/alea/strcuture.png?)
## Deploy

The project is online here: https://alea-frontend.vercel.app/
You can login with this email _michael.lawson@reqres.in_ with any password

## Status
Major part of the project is ready, only Unit Tests left, code can be reviewed

## Todos, challenges & improvements
- Writing services and injecting dependencies with SSR can be a little bit tricky because of environments. (window object in Node.js)
- Better handle for errors at high level
- Save token in memory instead of a cookie
- Better form validation
