import { AleaApp } from "@/constants";
import { UserAuth } from "@/services";

export function useUserAuthService(): InstanceType<typeof UserAuth> {
    return new UserAuth(
        AleaApp.Resources.cookieName,
        AleaApp.Resources.loginEndpoint,
        fetch,
        AleaApp.Resources.apiUrl,
    );
}