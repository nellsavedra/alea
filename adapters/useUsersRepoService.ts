import { AleaApp } from "@/constants";
import { UsersRepo } from "@/services";

export function useUsersRepoService(): InstanceType<typeof UsersRepo> {
    return new UsersRepo(
        AleaApp.Resources.usersEndpoint,
        fetch,
        AleaApp.Resources.apiUrl,
    );
}