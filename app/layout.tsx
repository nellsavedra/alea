import '@/styles/styles.scss';
import { headers } from "next/headers";
import { Roboto } from 'next/font/google';

export const runtime = "edge"

const roboto = Roboto({ weight: ['400', "700", "900"], subsets: ['latin'] });

export default function RootLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    const headersList = headers();
    const url = new URL(headersList.get('x-url') as string);
    const path = url.pathname === "/" ? "login" : `page${url.pathname.replace("/", "_")}`;

    return (
        <html lang="en" className={roboto.className}>
            <head>
            </head>
            <body className={path}>{children}</body>
        </html>
    );
}
