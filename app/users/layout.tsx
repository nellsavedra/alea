import { AleaApp } from "@/constants";
import { cookies } from "next/headers";
import { redirect } from "next/navigation";

export const runtime = "edge"

export default function UsersLayout({ children }: { children: React.ReactNode; }) {
    const _cookies = cookies();
    const userToken = _cookies.get(AleaApp.Resources.cookieName);
    if (!userToken) { return redirect("/"); }

    return (
        <>
            {children}
        </>
    );
}