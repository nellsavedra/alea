"use client";
import { isEmailValid } from "@nellsavedra/toolbox";
import { IUserToken } from "@/services";
import { QueryClient, QueryClientProvider, useQuery } from "@tanstack/react-query";
import { useRef } from "react";
import { useUserAuthService } from "@/adapters";

const queryClient = new QueryClient();

export function Login() {
    return (
        <QueryClientProvider client={queryClient}>
            <LoginInner />
            {/* <ReactQueryDevtools initialIsOpen={false} /> */}
        </QueryClientProvider>
    );
}

function LoginInner() {
    const formRef = useRef<HTMLFormElement>(null);
    const { login, setCookie } = useUserAuthService();
    const { isError, error, refetch, data, isFetching } = useQuery({
        queryKey: ['login'],
        queryFn: handleLogin,
        enabled: false,
        retry: 1,
    });

    async function handleLogin(): Promise<boolean> {
        const form: FormData = new FormData(formRef.current!);
        const email = form.get("email") as string;
        const password = form.get("password") as string;
        const valid: boolean = isEmailValid(email) && password.length >= 8;
        if (!valid) throw new Error("Invalid email or password");
        const { token }: Awaited<IUserToken> = await login(email, password);
        if (!token) throw new Error("Invalid email or password");
        setCookie(token);
        window.location.href = "/users";
        return true;
    };

    return (
        <main className="login__container">
            <h1 className="login__title">
                <img
                    alt="Alea Play"
                    className="logo"
                    src="/assets/svg/slot-machine.svg"
                />
            </h1>
            <form
                className="login__form"
                onSubmit={(event) => { event.preventDefault(); refetch(); }}
                ref={formRef}
            >
                <label className="login__form__label" htmlFor="email">
                    Email*
                    <input
                        className="login__form__input"
                        id="email"
                        name="email"
                        placeholder="example@alea.com"
                        type="email"
                    />
                </label>
                <label className="login__form__label" htmlFor="password">
                    Password*
                    <input
                        className="login__form__input"
                        id="password"
                        name="password"
                        placeholder="Your password"
                        type="password"
                    />
                </label>
                {
                    isError && !isFetching &&
                    <p className="login__form__error text-center">
                        {(error as Error).message}
                    </p>
                }
                <button
                    className={`login__form__button ${data ? "success" : ""}`}
                    type="submit"
                >
                    {
                        isFetching
                            ? "Loading..."
                            : data
                                ? "Redirecting..."
                                : "Login"
                    }
                </button>
            </form>
        </main>
    );
}