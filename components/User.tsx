import { IUser } from "@/services";

export const User = (user: IUser) => (
    <div key={user.id} className="users__row">
        <img className="users__avatar" src={`${user.avatar}`} />
        <h4 className="users__name">{`${user.first_name} ${user.last_name}`}</h4>
        <a className="users__email" href={`mailto:${user.email}`}>{user.email}</a>
    </div>
);