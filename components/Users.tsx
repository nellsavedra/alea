"use client";
import { AleaApp } from "@/constants";
import { IUser } from "@/services";
import { IUsersResponse } from "@/services";
import { QueryClient, QueryClientProvider, useQuery } from "@tanstack/react-query";
import { User } from "@/components";
import { useUserAuthService } from "@/adapters";
import { useUsersRepoService } from "@/adapters";
import { usePagination } from "@/hooks";

const queryClient = new QueryClient();

export function Users() {
    return (
        <QueryClientProvider client={queryClient}>
            <UsersInner />
            {/* <ReactQueryDevtools initialIsOpen={false} /> */}
        </QueryClientProvider>
    );
}

function UsersInner() {
    const { page, quantity, setPage, setQuantity } = usePagination();
    const { logout } = useUserAuthService();
    const { getUsers } = useUsersRepoService();
    const { data } = useQuery({
        queryKey: ['users', quantity, page],
        queryFn: fetchUsers,
        keepPreviousData: true,
        staleTime: 1000 * 60 * 5,
        cacheTime: 1000 * 60 * 30,
    });

    async function fetchUsers(): Promise<IUsersResponse> {
        const users: Awaited<IUsersResponse> = await getUsers(quantity, page);
        return users;
    }

    return (
        <main className="users">
            <header className="users__header">
                <h1 className="users__title">Top Players 🎖️</h1>
                {
                    data &&
                    <div className="users__filter">
                        <div>
                            <span>Show </span>
                            <select
                                id="quantity"
                                name="quantity"
                                onChange={(e) => setQuantity(parseInt(e.target.value))}
                                disabled={data.total_pages <= 0}
                            >
                                {
                                    AleaApp.Resources.viewsFilter.map(
                                        (view: number, index: number) => (
                                            <option
                                                key={index}
                                                value={view}
                                            >
                                                {view}
                                            </option>
                                        ))
                                }
                            </select>
                        </div>
                        <div>
                            <button
                                disabled={page <= 1}
                                onClick={() => setPage(page - 1)}
                            >
                                Prev
                            </button>
                            <span>
                                {` ${data.page} of ${data.total_pages} `}
                            </span>
                            <button
                                disabled={page >= data.total_pages}
                                onClick={() => setPage(page + 1)}
                            >
                                Next
                            </button>
                        </div>

                        <button
                            className="users__logout"
                            onClick={logout}
                        >
                            Logout
                        </button>
                    </div>
                }
            </header>
            {
                data &&
                <section className="users__table">
                    {
                        data.data.map((user: IUser) => (
                            <User key={user.id} {...user} />
                        ))
                    }
                </section>
            }
        </main>
    );
}