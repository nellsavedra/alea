export const AleaApp = {
    Resources: {
        apiUrl: "https://reqres.in",
        loginEndpoint: "/api/login",
        usersEndpoint: "/api/users",
        cookieName: "userToken",
        viewsFilter: [10, 5, 2, 1]
    }
};

