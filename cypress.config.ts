import { defineConfig } from "cypress";

export default defineConfig({
  e2e: {
    testIsolation: false,
    viewportWidth: 1280,
    viewportHeight: 720,
    supportFile: false,
    baseUrl: "http://localhost:3000",
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
