import { useEffect, useState } from "react";

export function usePagination() {
    const [page, setPage] = useState(1);
    const [quantity, setQuantity] = useState(10);

    useEffect(() => {
        setPage(1);
    }, [quantity]);

    return {
        page,
        setPage,
        quantity,
        setQuantity
    };
}