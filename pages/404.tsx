import '@/styles/src/_not-found.scss';
import { Roboto } from "next/font/google";
import React, { } from "react";

const roboto = Roboto({ weight: ['400', "700", "900"], subsets: ['latin'] });

export default function NotFound() {

    return (
        <main className={`error-page ${roboto.className}`}>
            <img
                alt="404 Not Found"
                className="error-page__image"
                src={"/assets/svg/404.svg"}
            />
            <h1 className="error-page__title">
                Page not found, best luck next time!
            </h1>
            <a className="error-page__back" href="/">Go back to home</a>
        </main>
    );
}