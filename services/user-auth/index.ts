import { IUserAuth } from "@/services";
import { IUserToken } from "@/services";

export class UserAuth implements IUserAuth {
    private readonly _cookieName: string;
    private readonly _endpoint: string;
    private readonly _fetcher: typeof window.fetch;
    private readonly _url: string;

    constructor(
        cookieName: string,
        endpoint: string,
        fetcher: typeof window.fetch,
        url: string,
    ) {
        this._cookieName = cookieName;
        this._endpoint = endpoint;
        this._fetcher = fetcher.bind(globalThis.window);
        this._url = url;
    }

    public login = async (email: string, password: string): Promise<IUserToken> => {
        const body = { email, password };
        const params: RequestInit = {
            method: "POST",
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response: Awaited<Response> = await this._fetcher(`${this._url}${this._endpoint}`, params);
        const { ok, status, statusText } = response;
        if (!ok) throw new Error(`Error ${status} ${statusText}`);
        const data: Awaited<IUserToken> = await response.json();
        return data;
    };

    public logout = (): void => {
        document.cookie = `${this._cookieName}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
        window.location.href = "/";
    };

    public setCookie = (token: string): void => {
        document.cookie = `${this._cookieName}=${token};path=/;secure`;
    };

    get cookieName(): string {
        return this._cookieName;
    }
}