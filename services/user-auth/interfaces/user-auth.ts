import { IUserToken } from "@/services";

export interface IUserAuth {
    login: (email: string, password: string) => Promise<IUserToken>;
    logout: () => void;
    setCookie: (token: string) => void;
}