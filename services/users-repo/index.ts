import { IUsersRepo } from "@/services";
import { IUsersResponse } from "@/services";


export class UsersRepo implements IUsersRepo {
    private readonly _endpoint: string;
    private readonly _fetcher: typeof window.fetch;
    private readonly _url: string;

    constructor(
        endpoint: string,
        fetcher: typeof window.fetch,
        url: string,
    ) {
        this._endpoint = endpoint;
        this._fetcher = fetcher.bind(globalThis.window);
        this._url = url;
    }

    getUsers = async (amount: number = 10, page: number = 1): Promise<IUsersResponse> => {
        const params: RequestInit = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response: Awaited<Response> = await this._fetcher(`${this._url}${this._endpoint}?per_page=${amount}&page=${page}`, params);
        const { ok, status, statusText } = response;
        if (!ok) throw new Error(`Error ${status} ${statusText}`);
        const data: Awaited<IUsersResponse> = await response.json();
        return data;
    };
}