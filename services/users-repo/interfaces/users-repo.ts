import { IUsersResponse } from "@/services";

export interface IUsersRepo {
    getUsers: (amount: number, page: number) => Promise<IUsersResponse>;
}