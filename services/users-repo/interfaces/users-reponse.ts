import { IUser } from "@/services";

export interface IUsersResponse {
    data: IUser[];
    page: number,
    per_page: number,
    total_pages: number,
    total: number,
}